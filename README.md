# Dxpchain API using PHP

The example shows how you can interface with Dxpchain' Blockchain API using PHP. 

### Environment and Requirements
+ Dxpchain v2
+ [websocket-php](https://github.com/Textalk/websocket-php)
+ A Running Full Node

### Installation
1. Download and install composer [here](https://getcomposer.org/download/)
2. Using your terminal, navigate to the location of your project, then run `composer install` 
3. Run your full node.
4. Open `index.php` then update RPC Endpoint URL to match your requirements. 

### References:
+ API Guide: http://docs.dxpchain.org/api/
+ Websocket Protocol and Syntax - http://docs.dxpchain.org/api/websocket.html 
+ Access Restricted API's: http://docs.dxpchain.org/api/access.html
+ Graphene App Method Reference: http://docs.dxpchain.org/development/namespaces/app.html

If you are looking for an example on how to interface with Dxpchain' **Wallet API** - [click here](https://gitlab.com/jessicaturner/dxpchain-api-cli-wallet)

## Author
#### Carl Victor C. Fontanos
+ Website: [carlofontanos.com](http://www.carlofontanos.com)
+ Linkedin: [ph.linkedin.com/in/carlfontanos](http://ph.linkedin.com/in/carlfontanos)
+ Facebook: [facebook.com/carlo.fontanos](http://facebook.com/carlo.fontanos)
+ Twitter: [twitter.com/carlofontanos](http://twitter.com/carlofontanos)
+ Google+: [plus.google.com/u/0/107219338853998242780/about](https://plus.google.com/u/0/107219338853998242780/about)
+ GitHub: [github.com/carlo-fontanos](https://github.com/carlo-fontanos)