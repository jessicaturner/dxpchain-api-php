<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'textalk/websocket' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../textalk/websocket',
            'aliases' => array(),
            'reference' => 'ba2e5f9ef7cf24d536d1c864ae74b2a9599b86eb',
            'dev_requirement' => false,
        ),
    ),
);
